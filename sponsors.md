---
title: QEMU sponsors
permalink: /sponsors/
---

QEMU has sponsors!

[Azure](https://azure.microsoft.com/) and [DigitalOcean](https://www.digitalocean.com/)
provide QEMU and [Patchew](https://patchew.org) with virtual machines and
other cloud resources through the [Azure credits for open source
projects](https://opensource.microsoft.com/azure-credits/) and [DigitalOcean
Open Source Credits](https://www.digitalocean.com/open-source/credits-for-projects)
programs.

[Equinix](https://www.arm.com/markets/computing-infrastructure/works-on-arm?#Equinix),
[IBM LinuxONE Community Cloud](https://developer.ibm.com/articles/get-started-with-ibm-linuxone/)
and the [Oregon State University Open Source Labs](https://www.osuosl.org)
also provide QEMU with access to compute hosts.

Downloads are hosted by [GNOME](https://gnome.org/).

QEMU is a member of the [GitLab for Open Source](https://about.gitlab.com/solutions/open-source/)
program.

You too can sponsor QEMU and be listed on this page; please contact the
maintainers on the [QEMU mailing list](mailto:qemu-devel@nongnu.org).
You can also [donate](https://paypal.com/donate/?hosted_button_id=YN74TZRMBBM6U)
to the project via PayPal.
